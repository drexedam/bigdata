package main

import (
	"bytes"
	"database/sql"
	"encoding/csv"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jakewins/neo4j-go"
	"log"
	"os"
	"strconv"
	"time"
)

type TestResult struct {
	MySQL int64
	Neo4J int64
}

var results map[int]*TestResult

const MAX_RUNS = 1000 //5 //1000
const MIN_INSERTS = 500

func main() {
	results = make(map[int]*TestResult)
	//ToDo Neo4J RAM lastig -> Ergebnisse schlechter wenn zuerst MySQL aufgerufen wird -> Testergebnisse nicht in map zwischenspeichern (-> mehr IO!!!)
	//	MySQLTests()
	Neo4JTests()
	MySQLTests()
	createCSV()
}

func MySQLTests() {
	db, err := sql.Open("mysql", "root:1234@/bigdatatest")
	checkError(err)

	qry := "INSERT INTO test (ID, CONTENT) VALUES "

	clearStmt, err := db.Prepare("DELETE FROM test")
	checkError(err)

	for run := MIN_INSERTS; run < MAX_RUNS; run++ {
		_, err = clearStmt.Exec()
		checkError(err)

		var buffer bytes.Buffer
		buffer.WriteString(qry)
		for i := 0; i < run; i++ {
			buffer.WriteString("(")
			buffer.WriteString(strconv.Itoa(i))
			buffer.WriteString(",")
			buffer.WriteString("'TEST'")
			buffer.WriteString(")")
			if i < run-1 {
				buffer.WriteString(",")
			}
		}

		stmt, err := db.Prepare(buffer.String())
		checkError(err)

		log.Printf("MYSQL: Run #%d", run)

		start := time.Now()
		_, err = stmt.Exec()
		elapsed := time.Since(start)

		checkError(err)

		log.Printf("MYSQL: Execution took %s", elapsed)

		if _, ok := results[run]; !ok {
			results[run] = &TestResult{}
		}
		res := results[run]
		res.MySQL = elapsed.Nanoseconds() / 1000000
	}

}

func Neo4JTests() {
	drive, err := neo4j.NewDriver("http://localhost:7474")
	checkError(err)

	sess, err := drive.NewSession()
	checkError(err)

	tx, err := sess.NewTransaction()
	checkError(err)

	qry := "CREATE "

	for run := MIN_INSERTS; run < MAX_RUNS; run++ {
		_, err = tx.Execute("MATCH (n) OPTIONAL MATCH (n)-[r]-() DELETE n,r")
		checkError(err)

		var buffer bytes.Buffer
		buffer.WriteString(qry)
		for i := 0; i < run; i++ {
			buffer.WriteString("(n")
			buffer.WriteString(strconv.Itoa(i))
			buffer.WriteString(":test {ID : ")
			buffer.WriteString(strconv.Itoa(i))
			buffer.WriteString(", CONTENT : 'TEST'})")
			if i < run-1 {
				buffer.WriteString(", ")
			}
		}

		log.Printf("NEO4J: Run #%d", run)

		start := time.Now()
		_, err = tx.Execute(buffer.String())
		elapsed := time.Since(start)

		checkError(err)

		log.Printf("NEO4J: Execution took %s", elapsed)

		if _, ok := results[run]; !ok {
			results[run] = &TestResult{}
		}
		res := results[run]
		res.Neo4J = elapsed.Nanoseconds() / 1000000

	}
}

func createCSV() {
	f, err := os.Create("./results.csv")
	checkError(err)

	defer f.Close()

	w := csv.NewWriter(f)
	var header []string
	header = append(header, "Inserted")
	header = append(header, "MySQL")
	header = append(header, "Neo4J")
	w.Write(header)
	for key, value := range results {
		var record []string
		record = append(record, strconv.Itoa(key))
		record = append(record, strconv.FormatInt(value.MySQL, 10))
		record = append(record, strconv.FormatInt(value.Neo4J, 10))
		w.Write(record)
	}

	w.Flush()
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
